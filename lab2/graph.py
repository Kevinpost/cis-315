#Author: Kevin Post
#Date: 4/17/2020
#CIS 315 Algorithms

#This python script takes in a file input containing a graph
# represented as a list of edges.
# Outputs:
# the total # of paths from node 1 to node N
# The shortest path from node 1 to node N
# The Longest path from node 1 to node N


import sys
import queue

class Graph:
    """Graph stores the graph as a dictionary of edges.
    Contains methods described in notes above"""
    def __init__(self, num_verts):
        """Initialize graph with number of vertices"""
        self.adjDict = dict()
        self.num_verts = num_verts


    def add_edge(self, a, b):
        """Add an edge to the dictionary"""
        if a in self.adjDict:
            self.adjDict[a].add(b)
        else:
            self.adjDict[a]= {b}

    def num_paths(self):
        """Calculates the number of total paths.
        Takes in a topologically sorted array of vertices"""
        numPath = []
        numPath.append(1)


        distance = []
        #initialize max_distance array
        for i in range(self.num_verts):
            distance.append(0)

        #initialize array tha will hold
        #num of paths
        for i in range(1, self.num_verts, 1):
            numPath.append(0)

        #iterate through vertices
        for i in range(self.num_verts-1):
            j = i+1
            for k in range(j, self.num_verts, 1):
                #if edge exists
                if (i+1) in self.adjDict:
                    if (k+1) in self.adjDict[i+1]:
                        #update num paths distance so far
                        numPath[k] += numPath[i]
                        #update longest path
                        if (distance[k] < (distance[i] +1)):
                            distance[k] = distance[i] +1
        #return num of paths & longest path
        return(distance[k], numPath[self.num_verts-1])

    def shortest_path(self):
        #basic BFS discussed in class
        q = queue.Queue()
        q.put(1)
        visited_nodes= set()
        visited_nodes.add(1)
        distance = 1
        distance_dict = dict()
        distance_dict[1] = 0

        while (not q.empty()):
            vertex = q.get()

            if (vertex in self.adjDict):
                for item in self.adjDict[vertex]:
                    if item not in visited_nodes:
                        #if new node encountered, increment distance
                        #to that node using its parent's distance value
                        # update its value in the distance dictionary
                        distance_dict[item] = distance_dict[vertex]+1
                        q.put(item)
                        visited_nodes.add(item)
        return distance_dict[self.num_verts]


def main():
    #open file given in stdin....
    f = open(sys.argv[1], 'r')

    #initialize a queue (passed to BFS for shortest path)
    q = queue.Queue()
    #read in the # of verts from opened file
    num_verts = int(f.readline())

    #initialize graph object
    graph = Graph(num_verts);

    #read in the number of edges from file
    num_edges = int(f.readline())
    for x in range(num_edges):
        #parse input and add each edge to graph
        line = f.readline()
        line = line.rstrip("\n")
        line = line.split(' ')
        line = [i for i in line]
        graph.add_edge(int(line[0]), int(line[1]))

    #get shortest path
    shortest_path = graph.shortest_path()

    #get longest path & number of paths
    distance, num_paths = graph.num_paths()

    #Make the output pretty
    print()
    print("——————————————")
    print(sys.argv[1])
    print("——————————————"+ "\n")
    print("Total number of paths: " + str(num_paths))
    print("Shortest path length: " + str(shortest_path))
    print("Longest path length: " + str(distance)+ "\n")

    return None



if (__name__ == "__main__"):
    main()
