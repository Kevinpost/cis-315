#Author: Kevin Post
#Date: 5/10/2020
#CIS 315 Algorithms

# <Notes> Makes change using dynamic programming algorithm to
# find the MAX number of coins, with a static amount of each coin.

import sys

makechange_cache = dict()


def makeChanger(denoms, target, num_denoms, num_left):
    table = [0 for i in range (target+1)]
    table[0] = 0
    for i in range(1, target+1):
        table[i] = -sys.maxsize
    num_left = []
    for i in range(num_denoms):
        num_left.append(5)

    for i in range(1, target+1):
        for j in range(num_denoms):
            if (denoms[j] <= i):
                sub_res = table[i-denoms[j]]
                if (sub_res != -sys.maxsize and sub_res + 1 > table[i]):
                    table[i] = sub_res + 1
    return table[target]

def makeChanger(denoms, target, num_denoms, num_left):
    max_coins = []
    for i in range(target+2):
        max_coins.append(-1)
    max_coins[0] = 0
    zero = 1
    tempcoins = -1
    for i in range(1, target+1):
        for j in range(num_denoms):
            for x in range(5):
                if denoms[j] <= i:
                    tempcoins = max_coins[i-denoms[j]]
                if (tempcoins + 1 > max_coins[i]):
                    max_coins[i] = tempcoins +1
    print(max_coins)
    return max_coins[target]

def makeChange(denoms, target, num_denoms, num_left, first_time):

    stringy = str(denoms) + str(target) + str(num_denoms) + str(num_left)

    if first_time == 0:
        num_left = [5 for i in range(num_denoms)]
        first_time = 1
    if stringy in makechange_cache:
        return makechange_cache[stringy]
    if target == 0:
        return 0
    result = -1
    for i in range(0, num_denoms):
            if (denoms[i] <= target):

                if (num_left[i] > 0 ):
                    num_left[i] -= 1
                    subresult = makeChange(denoms, target-denoms[i], num_denoms, num_left, first_time)
                    if (num_left[i] != -1):
                        if (subresult != -1 and subresult + 1 > result):
                            result = subresult + 1

    makechange_cache[stringy] = result
    return result



def main():
    #open file given in stdin....
    f = open(sys.argv[1], 'r')

    #read in the # of verts from opened file
    line = f.readline()
    line = line.rstrip("\n")
    line= line.split(' ')
    line = [i for i in line]
    num_coins, num_targets = int(line[0]), int(line[1])

    denoms = []
    num_denoms = 0
    for x in range(num_coins):
        #parse input and add each edge to graph
        line = f.readline()
        line = line.rstrip("\n")
        denoms.append(int(line))
        num_denoms += 1

    targets = []
    for x in range(num_targets):
        #parse input and add each edge to graph
        line = f.readline()
        line = line.rstrip("\n")
        targets.append(int(line))

    denoms.sort()
    num_left = []
    for x in range(num_denoms):
        num_left.append(5)

    first_time = 0
    for i in range(num_targets):
        result = makeChange(denoms, targets[i], num_denoms, num_left, first_time)
        if result == -1:
            print("target: " + str(targets[i]) + ", not possible")
        else:
            print("target: " + str(targets[i]) + ", max coins: " + str(result))

    return None



if (__name__ == "__main__"):
    main()
