import sys
import time
dictionary = set()
found_soln = 0

def wordify(line, string, length):
    global found_soln
    if (found_soln == 0):
        if (line == ""):
            found_soln = 1
            print("memoized attempt:\n----------YES, can be split----------\n"+string[1:] + "\n\n")
        for x in range(1, length):
            if line[:x] in dictionary:
                length -= len(line[:x])-2
                wordify(line[x:], string + ' ' + line[:x], length)

def main():

    dict_text = open('diction10k.txt', 'r')
    for x in range(58114):
        line = dict_text.readline()
        line = line.rstrip("\n")
        dictionary.add(line)
    num_lines = int(sys.stdin.readline())
    global found_soln
    line_num = 1
    timebefore = time.time()
    for x in range(num_lines):
        line = sys.stdin.readline()
        line = line.rstrip("\n")
        print("phrase number: " + str(line_num) + "\n" + line)
        length = len(line)
        wordify(line, "", length)
        if (found_soln == 0):
            print("memoized attempt:\n----------NO, cannot be split----------\n\n")
        found_soln = 0
        line_num += 1
    timeafter = time.time()
    elapsed = timeafter-timebefore
    print("Elapsed time: " + str(elapsed))
    return None
main()
