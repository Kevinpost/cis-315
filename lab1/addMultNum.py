import sys

def main():
    num_lines = int(sys.stdin.readline())
    #print(num_lines)
    for x in range(num_lines):
        line = sys.stdin.readline()
        line = line.rstrip("\n")
        line = line.split(' ')
        line = [int(i) for i in line]
        print(str((line[0]+line[1])) + ' ' + str((line[0]*line[1])))

    return None

main()
